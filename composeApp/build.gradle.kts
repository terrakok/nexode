import org.jetbrains.compose.desktop.application.dsl.TargetFormat

plugins {
    alias(libs.plugins.multiplatform)
    alias(libs.plugins.compose)
    alias(libs.plugins.cocoapods)
    alias(libs.plugins.android.application)
    alias(libs.plugins.buildConfig)
    alias(libs.plugins.dependencyUpdates)
}

@OptIn(org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi::class)
kotlin {
    targetHierarchy.default()
    android {
        compilations.all {
            kotlinOptions {
                jvmTarget = "11"
            }
        }
    }

    jvm("desktop")

    iosX64()
    iosArm64()
    iosSimulatorArm64()

    cocoapods {
        version = "1.0.0"
        summary = "Compose application framework"
        homepage = "empty"
        ios.deploymentTarget = "11.0"
        podfile = project.file("../iosApp/Podfile")
        framework {
            baseName = "ComposeApp"
            isStatic = true
        }
    }

    sourceSets {
        getByName("commonMain") {
            dependencies {
                implementation(compose.ui)
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material)
                implementation(libs.voyager.navigator)
                implementation(libs.napier)
                implementation(libs.kotlinx.coroutines.core)
                implementation(libs.composeIcons.featherIcons)
                implementation(libs.koin.core)
                implementation(libs.trixnity.client)
                implementation(libs.trixnity.client.media.okio)
                implementation(libs.trixnity.client.repository.realm)
                implementation(libs.ktor.client.core)
                implementation(libs.ktor.logging)
                implementation(libs.multiplatform.settings)
            }
        }

        getByName("androidMain") {
            dependencies {
                implementation(compose.uiTooling)
                implementation(libs.androidx.appcompat)
                implementation(libs.androidx.activityCompose)
                implementation(libs.compose.uitooling)
                implementation(libs.compose.uitooling.preview)
                implementation(libs.kotlinx.coroutines.android)
                implementation(libs.ktor.client.okhttp)
            }
        }

        getByName("desktopMain") {
            dependencies {
                implementation(compose.desktop.common)
                implementation(compose.desktop.currentOs)
                implementation(libs.ktor.client.okhttp)
                if (hasProperty("debug")) {
                    implementation(libs.logback.classic)
                }
            }
        }

        getByName("iosMain") {
            dependencies {
                implementation(libs.ktor.client.darwin)
            }
        }

        getByName("commonTest") {
            dependencies {
                implementation(kotlin("test"))
            }
        }
    }
}

android {
    namespace = "com.github.terrakok.nexode"
    compileSdk = 33

    defaultConfig {
        minSdk = 24
        targetSdk = 33

        applicationId = "com.github.terrakok.nexode.androidApp"
        versionCode = 1
        versionName = "1.0.0"
    }
    sourceSets["main"].apply {
        manifest.srcFile("src/androidMain/AndroidManifest.xml")
        res.srcDirs("src/androidMain/resources")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
}

compose {
    kotlinCompilerPlugin.set(dependencies.compiler.forKotlin("1.8.20"))
    kotlinCompilerPluginArgs.add("suppressKotlinVersionCompatibilityCheck=1.8.22")

    desktop {
        application {
            mainClass = "MainKt"

            nativeDistributions {
                targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
                packageName = "com.github.terrakok.nexode.desktopApp"
                packageVersion = "1.0.0"
            }
        }
    }
}

buildConfig {
    // BuildConfig configuration here.
    // https://github.com/gmazzo/gradle-buildconfig-plugin#usage-in-kts
    useKotlinOutput { internalVisibility = true }
    buildConfigField("boolean", "DEBUG", "${hasProperty("debug")}")
    buildConfigField("String", "DEFAULT_USER", "\"${findProperty("user") ?: ""}\"")
    buildConfigField("String", "DEFAULT_PWD", "\"${findProperty("password") ?: ""}\"")
}
