package com.github.terrakok.nexode

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.movableContentOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.Navigator
import compose.icons.FeatherIcons
import compose.icons.feathericons.Home
import compose.icons.feathericons.Moon
import compose.icons.feathericons.Search
import compose.icons.feathericons.Sun
import net.folivo.trixnity.client.MatrixClient
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class MainScreen : Screen, KoinComponent {
    override val key = uniqueScreenKey

    @Composable
    override fun Content() {
        val client: MatrixClient by inject()
        val isPhone by LocalPhoneMode.current
        var navigator: Navigator? = null

        val roomTree = remember {
            movableContentOf {
                RoomTree(
                    client,
                    modifier = Modifier.fillMaxSize(),
                    onRoomClick = {
                        navigator?.popUntilRoot()
                        navigator?.push(RoomScreen(it))
                    }
                )
            }
        }
        val roomBox = remember {
            movableContentOf {
                Navigator(EmptyRoomScreen) {
                    navigator = it
                    CurrentScreen()
                }
            }
        }

        if (!isPhone) {
            Row {
                VerticalMenu(client)
                Divider(modifier = Modifier.fillMaxHeight().width(1.dp))
                Box(
                    modifier = Modifier.fillMaxHeight().width(260.dp)
                ) {
                    roomTree()
                }
                Divider(modifier = Modifier.fillMaxHeight().width(1.dp))
                Box(
                    modifier = Modifier.fillMaxSize().weight(1f)
                ) {
                    roomBox()
                }
            }
        } else {
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                Column {
                    Box(
                        modifier = Modifier.fillMaxWidth().weight(1f)
                    ) {
                        roomTree()
                    }
                    Divider(modifier = Modifier.fillMaxWidth().height(1.dp))
                    HorizontalMenu(client)
                }
                Box(
                    modifier = Modifier.fillMaxSize()
                ) {
                    roomBox()
                }
            }
        }
    }
}

@Composable
private fun VerticalMenu(client: MatrixClient) {
    Column(Modifier.fillMaxHeight().width(50.dp)) {
        Icon(
            modifier = Modifier.size(50.dp).padding(16.dp),
            imageVector = FeatherIcons.Home,
            contentDescription = null
        )
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            modifier = Modifier.size(50.dp).padding(16.dp),
            imageVector = FeatherIcons.Search,
            contentDescription = null
        )
        ThemeSwitcher()
        ProfileState(client)
    }
}

@Composable
private fun HorizontalMenu(client: MatrixClient) {
    Row(Modifier.fillMaxWidth().height(50.dp)) {
        Icon(
            modifier = Modifier.size(50.dp).padding(16.dp),
            imageVector = FeatherIcons.Home,
            contentDescription = null
        )
        Spacer(modifier = Modifier.weight(1f))
        Icon(
            modifier = Modifier.size(50.dp).padding(16.dp),
            imageVector = FeatherIcons.Search,
            contentDescription = null
        )
        ThemeSwitcher()
        ProfileState(client)
    }
}

@Composable
private fun ThemeSwitcher() {
    var isDark by LocalThemeIsDark.current
    Icon(
        modifier = Modifier
            .size(50.dp)
            .clickable { isDark = !isDark }
            .padding(16.dp),
        imageVector = if (isDark) FeatherIcons.Sun else FeatherIcons.Moon,
        contentDescription = null
    )
}