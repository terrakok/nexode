package com.github.terrakok.nexode.domain

import com.github.terrakok.nexode.getCacheDirectoryPath
import com.russhwolf.settings.Settings
import com.russhwolf.settings.nullableString
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.http.Url
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.MatrixClientConfiguration
import net.folivo.trixnity.client.fromStore
import net.folivo.trixnity.client.login
import net.folivo.trixnity.client.media.okio.OkioMediaStore
import net.folivo.trixnity.client.store.repository.realm.createRealmRepositoriesModule
import net.folivo.trixnity.clientserverapi.model.authentication.IdentifierType

class SessionManager(settings: Settings) {
    private lateinit var coroutineScope: CoroutineScope
    var client: MatrixClient? = null
        private set
    private var deviceId by settings.nullableString("DEVICE_ID")

    suspend fun tryRestoreSession(): Boolean {
        Napier.d("Try restore session [$deviceId]")
        if (this::coroutineScope.isInitialized) {
            Napier.d("there was previous coroutine scope")
            coroutineScope.cancel()
        }
        coroutineScope = CoroutineScope(SupervisorJob())
        val restored = MatrixClient.fromStore(
            mediaStore = createMediaStore(),
            repositoriesModule = createRepositoriesModule(),
            scope = coroutineScope,
            configuration = clientConfig
        ).getOrNull()

        if (restored != null) {
            client = restored
            return true
        } else {
            return false
        }
    }

    suspend fun login(server: String, username: String, password: String) {
        Napier.d("Open session [$deviceId]")
        if (this::coroutineScope.isInitialized) {
            Napier.d("there was previous coroutine scope")
            coroutineScope.cancel()
        }
        coroutineScope = CoroutineScope(SupervisorJob())

        val url = if (server.startsWith("http")) server else "https://$server"
        client = MatrixClient.login(
            baseUrl = Url(url),
            scope = coroutineScope,
            mediaStore = createMediaStore(),
            repositoriesModule = createRepositoriesModule(),
            identifier = IdentifierType.User(username),
            password = password,
            deviceId = deviceId,
            configuration = clientConfig
        ).getOrThrow()
        deviceId = client?.deviceId
    }

    fun close() {
        Napier.d("Close session")
        coroutineScope.cancel()
        client = null
    }

    private fun createMediaStore() = OkioMediaStore(
        getCacheDirectoryPath().resolve("media")
    )

    private fun createRepositoriesModule() = createRealmRepositoriesModule {
        directory(getCacheDirectoryPath().resolve("realm").toString())
    }

    private val clientConfig: MatrixClientConfiguration.() -> Unit = {
        httpClientFactory = {
            HttpClient {
                it()
                install(Logging) {
                    level = LogLevel.ALL
                    logger = object : Logger {
                        override fun log(message: String) {
                            Napier.d(tag = "HTTP Client", message = message)
                        }
                    }
                }
            }
        }
    }
}