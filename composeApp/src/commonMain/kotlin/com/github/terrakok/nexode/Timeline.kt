package com.github.terrakok.nexode

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.TimelineState
import net.folivo.trixnity.client.room.getTimeline
import net.folivo.trixnity.client.store.TimelineEvent
import net.folivo.trixnity.client.store.avatarUrl
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.m.room.MemberEventContent
import net.folivo.trixnity.core.model.events.m.room.RoomMessageEventContent

@Composable
fun Timeline(
    roomId: RoomId,
    client: MatrixClient,
    modifier: Modifier = Modifier
) {
    Box(modifier) {
        val scope = rememberCoroutineScope()
        val timeline by remember { mutableStateOf(client.room.getTimeline(roomId)) }
        val timelineState by timeline.state.collectAsState(TimelineState())

        LaunchedEffect(timeline) {
            client.room.getById(roomId).firstOrNull()?.let { room ->
                room.lastEventId?.let { timeline.init(it) }
                if (!room.membersLoaded) {
                    client.user.loadMembers(roomId, false)
                }
            }
        }

        if (timelineState.isInitialized) {
            if (!timelineState.isLoadingAfter && timelineState.canLoadAfter) {
                scope.launch {
                    Napier.d("loadAfter [${roomId.full}]")
                    timeline.loadAfter()
                }
            }

            LazyColumn(
                modifier = Modifier.padding(horizontal = 8.dp),
                reverseLayout = true
            ) {
                if (timelineState.isLoadingAfter) {
                    item {
                        Box(modifier = Modifier.fillMaxWidth()) {
                            CircularProgressIndicator(
                                modifier = Modifier.padding(8.dp).align(Alignment.Center).size(16.dp)
                            )
                        }
                    }
                }
                itemsIndexed(timelineState.elements.reversed()) { index, element ->
                    if (index > timelineState.elements.size - 5 && !timelineState.isLoadingBefore && timelineState.canLoadBefore) {
                        scope.launch {
                            Napier.d("loadBefore [${roomId.full}]")
                            timeline.loadBefore()
                        }
                    }
                    Spacer(modifier = Modifier.size(8.dp))
                    TimelineElement(element, client)
                }

                item {
                    if (timelineState.isLoadingBefore) {
                        Box(modifier = Modifier.fillMaxWidth()) {
                            CircularProgressIndicator(
                                modifier = Modifier.padding(8.dp).align(Alignment.Center).size(16.dp)
                            )
                        }
                    } else {
                        Spacer(modifier = Modifier.size(8.dp))
                    }
                }
            }
        }
    }
}

@Composable
private fun TimelineElement(element: Flow<TimelineEvent>, client: MatrixClient) {
    val itemState by element.collectAsState(null)
    val item = itemState
    if (item != null) {
        val itemText = when (val content = item.content?.getOrNull()) {
            is RoomMessageEventContent.TextMessageEventContent -> content.body
            is MemberEventContent -> content.membership.value
            else -> "[${content?.let { it::class.simpleName }}]"
        }

        val isMy = item.event.sender == client.userId
        val shape = RoundedCornerShape(
            topStart = 8.dp,
            topEnd = 8.dp,
            bottomStart = if (isMy) 8.dp else 0.dp,
            bottomEnd = if (!isMy) 8.dp else 0.dp
        )
        val padding = PaddingValues(
            start = if (isMy) 40.dp else 0.dp,
            top = 0.dp,
            end = if (isMy) 0.dp else 40.dp,
            bottom = 0.dp
        )

        val userState by client.user.getById(item.roomId, item.event.sender).collectAsState(null)

        Row {
            if (!isMy) {
                Box(
                    modifier = Modifier.align(Alignment.Bottom).padding(end = 8.dp).size(32.dp)
                ) {
                    userState?.let { user ->
                        Avatar(Modifier.fillMaxSize(), client, user.avatarUrl, user.name)
                    }
                }
            } else {
                Spacer(modifier = Modifier.weight(1f))
            }
            Text(
                modifier = Modifier
                    .padding(padding)
                    .widthIn(min = 100.dp, max = 400.dp)
                    .clip(shape)
                    .background(MaterialTheme.colors.primary)
                    .padding(16.dp),
                text = itemText,
                color = MaterialTheme.colors.onPrimary
            )
            if (isMy) {
                Box(
                    modifier = Modifier.align(Alignment.Bottom).padding(start = 8.dp).size(32.dp)
                ) {
                    userState?.let { user ->
                        Avatar(Modifier.fillMaxSize(), client, user.avatarUrl, user.name)
                    }
                }
            }
        }
    } else {
        Spacer(modifier = Modifier.size(40.dp))
    }
}