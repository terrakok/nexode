package com.github.terrakok.nexode

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import com.github.terrakok.nexode.DialogState.Companion.rememberDialogState
import kotlinx.coroutines.launch
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.clientserverapi.client.SyncState

@Composable
fun ProfileState(client: MatrixClient) {
    val avatarUrl by client.avatarUrl.collectAsState()
    val username by client.displayName.collectAsState()
    val syncState by client.syncState.collectAsState()
    val navigator = LocalNavigator.currentOrThrow

    val logoutDialog = rememberDialogState(data = "Do you want to log out?")
    val scope = AppScope
    QuestionDialog(logoutDialog) {
        scope.launch {
            client.cancelSync()
            client.logout()
            client.clearCache()
            client.clearMediaCache()
        }
        navigator.replaceAll(LoginScreen())
    }

    Box(
        modifier = Modifier
            .size(50.dp)
            .clickable { logoutDialog.visible = true },
        contentAlignment = Alignment.Center
    ) {
        Avatar(
            modifier = Modifier.size(32.dp),
            client = client,
            avatarUrl = avatarUrl,
            username = username.orEmpty()
        )
        val statusColor = when(syncState) {
            SyncState.INITIAL_SYNC,
            SyncState.STARTED,
            SyncState.RUNNING,
            SyncState.TIMEOUT -> Color.Transparent
            SyncState.ERROR -> Color.Red
            SyncState.STOPPING -> Color.LightGray
            SyncState.STOPPED -> Color.Gray
        }
        Box(
            modifier = Modifier
                .size(28.dp)
                .padding(10.dp)
                .clip(CircleShape)
                .background(statusColor)
                .align(Alignment.TopEnd)
        )
    }
}