package com.github.terrakok.nexode

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.Stable
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

private class OverlayRefs {
    val refs = mutableStateListOf<@Composable () -> Unit>()
}

private val LocalOverlayContainer = compositionLocalOf { OverlayRefs() }

@Composable
fun OverlayContainer(
    content: @Composable () -> Unit
) {
    val overlays = OverlayRefs()

    CompositionLocalProvider(
        LocalOverlayContainer provides overlays
    ) {
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            content()
            for (composable in overlays.refs) {
                composable()
            }
        }
    }
}

@Stable
class DialogState<T> private constructor(val disposable: Boolean, visibility: Boolean, data: T?) {
    var visible by mutableStateOf(visibility)
    var data: T? by mutableStateOf(data)

    companion object {
        @Composable
        fun <T> rememberDialogState(
            disposable: Boolean = true,
            visibility: Boolean = false,
            data: T? = null
        ) = remember { DialogState(disposable, visibility, data) }
    }
}

@Composable
fun <T> OverlayDialog(
    state: DialogState<T>,
    content: @Composable () -> Unit
) {
    val container = LocalOverlayContainer.current
    var dialog: (@Composable () -> Unit)? = null
    dialog = {
        Box(
            modifier = Modifier
                .background(Color.Gray.copy(alpha = 0.5f))
                .fillMaxSize()
                .clickable(
                    interactionSource = MutableInteractionSource(),
                    indication = null,
                    onClick = {
                        if (state.disposable) state.visible = false
                    }
                ),
            contentAlignment = Alignment.Center
        ) {
            Box(
                modifier = Modifier
                    .clickable(
                        interactionSource = MutableInteractionSource(),
                        indication = null,
                        onClick = {}
                    ),
                contentAlignment = Alignment.Center
            ) {
                content()
            }

            if (!state.visible) {
                container.refs.remove(dialog)
            }
        }
    }

    if (state.visible) {
        container.refs.add(dialog)
    }
}