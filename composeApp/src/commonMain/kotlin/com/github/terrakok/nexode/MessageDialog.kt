package com.github.terrakok.nexode

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MessageDialog(state: DialogState<String>) = OverlayDialog(state) {
    Card(
        modifier = Modifier.width(400.dp).padding(16.dp),
        elevation = 4.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Column {
            Text(
                modifier = Modifier.padding(16.dp),
                text = state.data.orEmpty(),
                style = MaterialTheme.typography.subtitle1
            )
            Spacer(modifier = Modifier.size(8.dp))
            Row(
                modifier = Modifier.align(Alignment.End).padding(horizontal = 8.dp)
            ) {
                TextButton(
                    onClick = { state.visible = false }
                ) {
                    Text(text = "OK")
                }
            }
        }
    }
}

@Composable
fun QuestionDialog(state: DialogState<String>, onConfirm: () -> Unit) = OverlayDialog(state) {
    Card(
        modifier = Modifier.width(400.dp).padding(16.dp),
        elevation = 4.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Column {
            Text(
                modifier = Modifier.padding(16.dp),
                text = state.data.orEmpty(),
                style = MaterialTheme.typography.subtitle1
            )
            Spacer(modifier = Modifier.size(8.dp))
            Row(
                modifier = Modifier.align(Alignment.End).padding(horizontal = 8.dp)
            ) {
                TextButton(
                    onClick = { state.visible = false }
                ) {
                    Text(text = "NO")
                }
                Spacer(modifier = Modifier.size(8.dp))
                TextButton(
                    onClick = {
                        onConfirm()
                        state.visible = false
                    }
                ) {
                    Text(text = "YES")
                }
            }
        }
    }
}