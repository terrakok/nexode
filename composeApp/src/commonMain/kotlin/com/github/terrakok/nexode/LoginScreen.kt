package com.github.terrakok.nexode

import Nexode.composeApp.BuildConfig
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import com.github.terrakok.nexode.DialogState.Companion.rememberDialogState
import com.github.terrakok.nexode.domain.SessionManager
import compose.icons.FeatherIcons
import compose.icons.feathericons.Eye
import compose.icons.feathericons.EyeOff
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class LoginScreen : Screen, KoinComponent {
    override val key = uniqueScreenKey

    @Composable
    override fun Content() {
        val sessionManager: SessionManager by inject()
        val scope = rememberCoroutineScope()
        val navigator = LocalNavigator.currentOrThrow
        var server by remember { mutableStateOf("matrix.org") }
        var username by remember { mutableStateOf(BuildConfig.DEFAULT_USER) }
        var password by remember { mutableStateOf(BuildConfig.DEFAULT_PWD) }

        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Card(
                modifier = Modifier.width(400.dp).padding(24.dp),
                elevation = 8.dp
            ) {
                Column(
                    modifier = Modifier.padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    OutlinedTextField(
                        value = server,
                        onValueChange = { server = it },
                        label = { Text("Homeserver") },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.size(16.dp))
                    OutlinedTextField(
                        value = username,
                        onValueChange = { username = it },
                        label = { Text("Username") },
                        modifier = Modifier.fillMaxWidth(),
                        maxLines = 1,
                        singleLine = true
                    )
                    Spacer(modifier = Modifier.size(16.dp))
                    PasswordTextField(
                        password = password,
                        onPasswordChange = { password = it },
                        modifier = Modifier.fillMaxWidth()
                    )
                    Spacer(modifier = Modifier.size(16.dp))

                    var isProgress by remember { mutableStateOf(false) }
                    val msgDialog = rememberDialogState<String>()
                    MessageDialog(msgDialog)

                    ProgressButton(
                        modifier = Modifier.align(Alignment.End),
                        enabled = server.isNotBlank() && username.isNotBlank() && password.isNotBlank(),
                        progress = isProgress,
                        text = "Login",
                        onClick = {
                            scope.launch {
                                isProgress = true
                                try {
                                    sessionManager.login(server, username, password)
                                    navigator.replace(AuthorizedScreen())
                                } catch (e: Exception) {
                                    msgDialog.visible = true
                                    msgDialog.data = e.message
                                }
                                isProgress = false
                            }
                        }
                    )
                }
            }
        }
    }

    @Composable
    private fun PasswordTextField(
        password: String,
        onPasswordChange: (String) -> Unit,
        modifier: Modifier = Modifier,
        label: String = "Password"
    ) {
        var passwordVisibility by remember { mutableStateOf(false) }

        OutlinedTextField(
            value = password,
            onValueChange = onPasswordChange,
            modifier = modifier,
            maxLines = 1,
            singleLine = true,
            label = { Text(label) },
            visualTransformation = if (passwordVisibility) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Password
            ),
            trailingIcon = {
                IconButton(onClick = { passwordVisibility = !passwordVisibility }) {
                    val imageVector = if (passwordVisibility) FeatherIcons.Eye else FeatherIcons.EyeOff
                    Icon(imageVector, contentDescription = if (passwordVisibility) "Hide password" else "Show password")
                }
            }
        )
    }

    @Composable
    private fun ProgressButton(
        modifier: Modifier,
        text: String,
        progress: Boolean,
        enabled: Boolean,
        onClick: () -> Unit
    ) {
        Button(
            modifier = modifier,
            onClick = onClick,
            enabled = enabled && !progress
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                if (progress) {
                    CircularProgressIndicator(
                        modifier = Modifier.size(16.dp),
                        strokeWidth = 1.dp
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                }
                Text(text)
            }
        }
    }
}