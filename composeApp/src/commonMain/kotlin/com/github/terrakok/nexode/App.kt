package com.github.terrakok.nexode

import Nexode.composeApp.BuildConfig
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import cafe.adriel.voyager.navigator.Navigator
import com.github.terrakok.nexode.domain.SessionManager
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.mp.KoinPlatform

private val LocalAppScope = compositionLocalOf<CoroutineScope> { GlobalScope }
val AppScope @Composable get() = LocalAppScope.current

@Composable
fun App() {
    initApp()

    Napier.d("Start application")
    val sessionManager: SessionManager by KoinPlatform.getKoin().inject()

    DisposableEffect(Unit) {
        onDispose {
            Napier.d("Stop application")
            sessionManager.close()
        }
    }

    CompositionLocalProvider(
        LocalAppScope provides rememberCoroutineScope()
    ) {
        AppTheme {
            OverlayContainer {
                var hasPreviousSession by remember { mutableStateOf<Boolean?>(null) }
                LaunchedEffect(Unit) {
                    hasPreviousSession = sessionManager.tryRestoreSession()
                }
                val screen = when (hasPreviousSession) {
                    true -> AuthorizedScreen()
                    false -> LoginScreen()
                    else -> null
                }
                if (screen != null) {
                    Navigator(screen)
                }
            }
        }
    }
}

private var isInit = false
private fun initApp() {
    if (isInit) return
    isInit = true

    if (BuildConfig.DEBUG) {
        Napier.base(DebugAntilog())
    }

    val appModule = module {
        single { getPlatformSettings() }
        single { SessionManager(get()) }
        factory { get<SessionManager>().client!! }
    }
    startKoin {
        modules(appModule)
    }
}
