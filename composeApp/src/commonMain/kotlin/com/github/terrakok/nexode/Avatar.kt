package com.github.terrakok.nexode

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import net.folivo.trixnity.client.MatrixClient

@Composable
fun Avatar(
    modifier: Modifier = Modifier,
    client: MatrixClient,
    avatarUrl: String?,
    username: String
) {
    val imageModifier = Modifier
        .clip(CircleShape)
        .background(color = MaterialTheme.colors.primary)
        .then(modifier)

    if (avatarUrl != null) {
        MatrixImage(
            modifier = imageModifier,
            mxcUri = avatarUrl,
            client = client
        )
    } else {
        Box(
            modifier = imageModifier,
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = getInitials(username),
                color = MaterialTheme.colors.onPrimary
            )
        }
    }
}