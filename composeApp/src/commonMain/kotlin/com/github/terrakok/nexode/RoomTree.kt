package com.github.terrakok.nexode

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.getStateKey
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.room.RoomService
import net.folivo.trixnity.client.room.flatten
import net.folivo.trixnity.client.room.getAllState
import net.folivo.trixnity.client.room.getState
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.RoomId
import net.folivo.trixnity.core.model.events.m.room.CreateEventContent
import net.folivo.trixnity.core.model.events.m.space.ChildEventContent
import net.folivo.trixnity.core.model.events.m.space.ParentEventContent

@Composable
fun RoomTree(
    client: MatrixClient,
    modifier: Modifier = Modifier,
    onRoomClick: (RoomId) -> Unit
) {
    val roots by client.room.getAll().flatten()
        .map { rooms ->
            rooms
                .filter { client.room.isRoot(it.roomId) }
                .map { it to client.room.isSpace(it.roomId) }
                .sortedWith { a, b ->
                    val (aRoom, aSpace) = a
                    val (bRoom, bSpace) = b

                    if (aSpace && bSpace) 0
                    else if (aSpace) -1
                    else if (bSpace) 1
                    else if (aRoom.isDirect && bRoom.isDirect) 0
                    else if (aRoom.isDirect) 1
                    else if (bRoom.isDirect) -1
                    else 0
                }
                .map { (room, isSpace) -> room }
        }
        .collectAsState(emptySet())

    Column(
        modifier = modifier.verticalScroll(rememberScrollState())
    ) {
        roots.forEach { room -> RoomTreeItem(client, room, onRoomClick) }
    }
}

private suspend fun RoomService.isSpace(roomId: RoomId) =
    getState<CreateEventContent>(roomId).first()?.content?.type == CreateEventContent.RoomType.Space

private suspend fun RoomService.isRoot(roomId: RoomId) =
    getAllState<ParentEventContent>(roomId)
        .map { events -> events.orEmpty().values.isEmpty() }
        .first()

private fun RoomService.getRoomChildren(roomId: RoomId) =
    getAllState<ChildEventContent>(roomId).map { events ->
        events.orEmpty().values
            .filterNotNull()
            .sortedBy { event -> event.content.order }
            .mapNotNull { event -> event.getStateKey()?.let { RoomId(it) } }
    }

@Composable
private fun RoomTreeItem(
    client: MatrixClient,
    room: Room,
    onRoomClick: (RoomId) -> Unit,
    level: Int = 0
) {
    var isSpace by remember { mutableStateOf<Boolean?>(null) }
    LaunchedEffect(room.roomId) {
        isSpace = client.room.isSpace(room.roomId)
    }

    Column {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .clickable(enabled = isSpace != true) {
                    onRoomClick(room.roomId)
                }
                .padding(start = 12.dp * level)
        ) {
            when (isSpace) {
                true -> SpaceHeader(client, room)
                false -> RoomHeader(client, room)
                else -> {}
            }
        }

        if (isSpace == true) {
            val children by client.room.getRoomChildren(room.roomId).collectAsState(emptyList())
            Column {
                children.forEach { roomId ->
                    val child by client.room.getById(roomId).collectAsState(null)
                    child?.let { RoomTreeItem(client, it, onRoomClick, level + 1) }
                }
            }
        }
    }
}

@Composable
private fun SpaceHeader(client: MatrixClient, room: Room) {
    Row(
        modifier = Modifier.padding(start = 12.dp, top = 8.dp, end = 8.dp, bottom = 8.dp)
    ) {
        val imageUrl = room.avatarUrl
        val title = room.name?.let { it.explicitName ?: it.heroes.joinToString() } ?: room.roomId.localpart
        val modifier = Modifier
            .clip(RoundedCornerShape(20))
            .size(24.dp)
        if (imageUrl != null) {
            MatrixImage(modifier.background(color = Color.White), client, imageUrl)
        } else {
            Box(
                modifier = modifier.background(color = MaterialTheme.colors.primary)
            ) {
                Text(
                    modifier = Modifier.align(Alignment.Center),
                    text = "#",
                    color = MaterialTheme.colors.onPrimary,
                    style = MaterialTheme.typography.caption
                )
            }
        }
        Spacer(modifier = Modifier.size(12.dp))
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterVertically)
                .weight(1f),
            maxLines = 1,
            text = title,
            softWrap = false,
            overflow = TextOverflow.Ellipsis,
            style = MaterialTheme.typography.caption
        )
    }
}

@Composable
private fun RoomHeader(client: MatrixClient, room: Room) {
    Row(
        modifier = Modifier.padding(start = 8.dp, top = 8.dp, end = 8.dp, bottom = 8.dp)
    ) {
        val imageUrl = room.avatarUrl
        val title by room.getTitle(client.user).collectAsState("")
        val modifier = Modifier
            .clip(CircleShape)
            .size(32.dp)
        if (imageUrl != null) {
            MatrixImage(modifier.background(color = Color.White), client, imageUrl)
        } else {
            Box(
                modifier = modifier.background(color = MaterialTheme.colors.primary)
            ) {
                Text(
                    modifier = Modifier.align(Alignment.Center),
                    text = getInitials(title),
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }
        Spacer(modifier = Modifier.size(8.dp))
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterVertically)
                .weight(1f),
            maxLines = 1,
            style = MaterialTheme.typography.body2,
            text = title,
            softWrap = false,
            overflow = TextOverflow.Ellipsis
        )
    }
}