package com.github.terrakok.nexode

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.requiredSizeIn
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp

private val LightColors = lightColors(
    primary = md_theme_light_primary,
    onPrimary = md_theme_light_onPrimary,
    secondary = md_theme_light_secondary,
    onSecondary = md_theme_light_onSecondary,
    error = md_theme_light_error,
    onError = md_theme_light_onError,
    background = md_theme_light_background,
    onBackground = md_theme_light_onBackground,
    surface = md_theme_light_surface,
    onSurface = md_theme_light_onSurface,
)

private val DarkColors = darkColors(
    primary = md_theme_dark_primary,
    onPrimary = md_theme_dark_onPrimary,
    secondary = md_theme_dark_secondary,
    onSecondary = md_theme_dark_onSecondary,
    error = md_theme_dark_error,
    onError = md_theme_dark_onError,
    background = md_theme_dark_background,
    onBackground = md_theme_dark_onBackground,
    surface = md_theme_dark_surface,
    onSurface = md_theme_dark_onSurface,
)

val LocalThemeIsDark = compositionLocalOf { mutableStateOf(true) }
val LocalPhoneMode = compositionLocalOf { mutableStateOf(false) }

@Composable
fun AppTheme(
    content: @Composable() () -> Unit
) {
    CompositionLocalProvider(
        LocalThemeIsDark provides mutableStateOf(isSystemInDarkTheme())
    ) {
        val isDark by LocalThemeIsDark.current
        var isPhone by LocalPhoneMode.current
        val colors = if (!isDark) LightColors else DarkColors
        MaterialTheme(
            colors = colors,
            content = {
                val density = LocalDensity.current
                Box(
                    modifier = Modifier
                        .requiredSizeIn(minWidth = 300.dp, minHeight = 200.dp)
                        .fillMaxSize()
                        .onGloballyPositioned {
                            with(density) {
                                isPhone = it.size.width.toDp() <= 800.dp
                            }
                        }
                ) {
                    Surface(content = content)
                }
            }
        )
    }
}
