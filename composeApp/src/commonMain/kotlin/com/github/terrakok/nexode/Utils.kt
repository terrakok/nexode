package com.github.terrakok.nexode

import androidx.compose.ui.graphics.ImageBitmap
import com.russhwolf.settings.Settings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.runningFold
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.client.user.UserService
import okio.Path

expect fun openUrl(url: String?)
expect fun getPlatformSettings(): Settings
expect fun getCacheDirectoryPath(): Path
expect fun ByteArray.asImageBitmap(): ImageBitmap

fun getInitials(name: String): String {
    val words = name.split(" ")
    return if (words.size > 1) {
        "${words[0].take(1)}${words[1].take(1)}"
    } else {
        words[0].take(2)
    }.uppercase()
}

fun Room?.getTitle(userService: UserService): Flow<String> {
    val name = this?.name ?: return flowOf(this?.roomId?.full.orEmpty())
    val explicitName = name.explicitName
    if (explicitName != null) return flowOf(explicitName)

    return name.heroes
        .map { userId -> userService.getById(this.roomId, userId).filterNotNull() }
        .asFlow()
        .flattenMerge()
        .runningFold(mutableMapOf<String, String>()) { acc, value ->
            acc[value.userId.full] = value.name
            acc
        }
        .map { acc -> acc.values.joinToString() }
}