package com.github.terrakok.nexode

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import compose.icons.FeatherIcons
import compose.icons.feathericons.ArrowLeft
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.room
import net.folivo.trixnity.client.store.Room
import net.folivo.trixnity.client.user
import net.folivo.trixnity.core.model.RoomId
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class RoomScreen(val roomId: RoomId) : Screen, KoinComponent {
    override val key = roomId.full

    @Composable
    override fun Content() {
        val client: MatrixClient by inject()
        val navigator = LocalNavigator.currentOrThrow
        val room by client.room.getById(roomId).collectAsState(Room(roomId))
        val isPhone by LocalPhoneMode.current

        Column(
            modifier = Modifier.fillMaxSize()
                .background(MaterialTheme.colors.surface)
                .clickable(enabled = false, onClick = {})
        ) {
            TopAppBar(
                navigationIcon = if (isPhone) {
                    {
                        IconButton(onClick = { navigator.pop() }) {
                            Icon(imageVector = FeatherIcons.ArrowLeft, contentDescription = null)
                        }
                    }
                } else null,
                title = {
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        val imageUrl = room?.avatarUrl
                        if (imageUrl != null) {
                            MatrixImage(
                                Modifier
                                    .clip(CircleShape)
                                    .size(32.dp)
                                    .background(color = Color.White),
                                client,
                                imageUrl
                            )
                            Spacer(modifier = Modifier.size(16.dp))
                        }
                        val title by room.getTitle(client.user).collectAsState("")
                        Text(title)
                    }
                },
                elevation = 16.dp
            )
            Timeline(roomId, client, modifier = Modifier.weight(1f))
            Divider(modifier = Modifier.fillMaxWidth().height(1.dp))
            Row(
                verticalAlignment = Alignment.Bottom
            ) {
                IconButton(
                    modifier = Modifier.padding(6.dp).size(40.dp),
                    onClick = {}
                ) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = null
                    )
                }
                var msgText by remember { mutableStateOf("") }
                val focusRequester = remember { FocusRequester() }
                LaunchedEffect(Unit) {
                    focusRequester.requestFocus()
                }
                BasicTextField(
                    modifier = Modifier
                        .weight(1f)
                        .heightIn(min = 40.dp, max = 200.dp)
                        .padding(4.dp)
                        .focusRequester(focusRequester),
                    value = msgText,
                    onValueChange = { msgText = it },
                    textStyle = MaterialTheme.typography.body1
                )
                IconButton(
                    modifier = Modifier.padding(6.dp).size(40.dp),
                    onClick = {
                        msgText = ""
                        focusRequester.requestFocus()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Default.Send,
                        contentDescription = null,
                        tint = MaterialTheme.colors.primary
                    )
                }
            }
        }
    }
}

object EmptyRoomScreen : Screen {
    @Composable
    override fun Content() {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            val isPhone by LocalPhoneMode.current
            if (!isPhone) {
                Text("Select room")
            }
        }
    }
}