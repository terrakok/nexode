package com.github.terrakok.nexode

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import com.russhwolf.settings.PreferencesSettings
import com.russhwolf.settings.Settings
import okio.Path
import okio.Path.Companion.toPath
import org.jetbrains.skia.Image
import java.awt.Desktop
import java.net.URI
import java.util.prefs.Preferences

actual fun openUrl(url: String?) {
    val uri = url?.let { URI.create(it) } ?: return
    Desktop.getDesktop().browse(uri)
}

private object NexodePreferences

actual fun getPlatformSettings(): Settings = PreferencesSettings(
    Preferences.userNodeForPackage(NexodePreferences::class.java)
)

actual fun getCacheDirectoryPath(): Path = System.getProperty("user.home").toPath().resolve(".nexode/cache")

actual fun ByteArray.asImageBitmap(): ImageBitmap =
    Image.makeFromEncoded(this).toComposeImageBitmap()
