package com.github.terrakok.nexode

import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import com.russhwolf.settings.NSUserDefaultsSettings
import com.russhwolf.settings.Settings
import okio.Path
import okio.Path.Companion.toPath
import org.jetbrains.skia.Image
import platform.Foundation.NSCachesDirectory
import platform.Foundation.NSSearchPathForDirectoriesInDomains
import platform.Foundation.NSURL
import platform.Foundation.NSUserDefaults
import platform.Foundation.NSUserDomainMask
import platform.UIKit.UIApplication

actual fun openUrl(url: String?) {
    val nsUrl = url?.let { NSURL.URLWithString(it) } ?: return
    UIApplication.sharedApplication.openURL(nsUrl)
}

actual fun getPlatformSettings(): Settings = NSUserDefaultsSettings(
    NSUserDefaults("NexodeSettings")
)

actual fun getCacheDirectoryPath(): Path {
    val cacheDir = NSSearchPathForDirectoriesInDomains(
        NSCachesDirectory,
        NSUserDomainMask,
        true
    ).first() as String
    return (cacheDir + "/cache").toPath()
}

actual fun ByteArray.asImageBitmap(): ImageBitmap =
    Image.makeFromEncoded(this).toComposeImageBitmap()
